const { argv } = process
const build = argv[argv.length - 1] === 'build'
const dev = argv.includes('-O')
const lib = require('./lib')

const FileStoreMiddle = [
    conf => lib(conf, {
        onSessionChange: (sessions) => {
            console.log(sessions)
        },
        onAddUser: (req) => {
            console.log('addUser', req.post)
        },
        use_avatar: true,
        auth_level: ['读取', '新增', '修改', '配置'],
        dev,
        login_captcha: {
            size: 5,
            noise: 2,
            color: true,
        }
    })
];

// @ts-check
/**
 * @type {import('f2e-server').F2EConfig}
 */
const config = {
    livereload: !build,
    gzip: true,
    build,
    buildFilter: (p) => !p || /src/.test(p),
    renderHeaders: (headers) => {
        return {
            ...headers,
            'X-Frame-Options': 'Deny',
        }
    },
    middlewares: [
        dev && { middleware: 'esbuild', test: /^\/?lib/ },
    ].concat(FileStoreMiddle)
}

module.exports = config