import { Authority, Role, User } from "../serve/interface"

export interface StoreState {
    curTab: string
    loginUser: User
    authorities: Authority[]
    roles: Role[]
    users: User[]
}