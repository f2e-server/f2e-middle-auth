import * as React from 'react'
import { Menu, Card, Layout } from 'antd'
import {
    TeamOutlined,
    SettingOutlined,
    UserOutlined
} from '@ant-design/icons';
import { connect, getState } from '../store'
import { User } from '../../serve/interface'
import RoleList from '../containers/RoleList'
import UserList from '../containers/UserList'
import UserCenter from '../containers/UserCenter'

const { Sider, Content } = Layout;

type TabKey = 'my' | 'role_list' | 'user_list'

const ADMINTABS: {
    id: TabKey,
    title?: string,
    icon?: object
}[] = [
        {
            id: 'my',
            title: '个人设置',
            icon: <UserOutlined />
        },
        {
            id: 'role_list',
            title: '角色管理',
            icon: <SettingOutlined />
        },
        {
            id: 'user_list',
            title: '用户管理',
            icon: <TeamOutlined />
        }
    ]

interface AdminProps {
    loginUser: User
}

interface AdminState {
    active_tab: string
}

class Admin extends React.Component<AdminProps, AdminState> {
    constructor(props: AdminProps) {
        super(props)
        this.state = {
            active_tab: 'my'
        }

    }

    menuOnclick = (e) => {
        this.setState({
            active_tab: e.key
        }
        )
    }

    render() {
        const { loginUser } = this.props
        let { active_tab } = this.state

        if (!loginUser) {
            return <div></div>
        }

        const tabs = loginUser.role.isAdmin ? ADMINTABS : ADMINTABS.slice(0, 1)

        return (
            <Layout style={{ height: '100vh' }}>
                <Layout>
                    <Sider theme="dark" breakpoint='lg' collapsedWidth={0}>
                        <Menu
                            theme="dark"
                            mode="inline"
                            defaultSelectedKeys={[active_tab]}
                            onClick={e => this.menuOnclick(e)}
                        >
                            {
                                tabs.map(t => {
                                    return <Menu.Item key={t.id} icon={t.icon}>
                                        {t.title}
                                    </Menu.Item>
                                })
                            }
                        </Menu>
                    </Sider>

                    <Layout className="site-layout">
                        <Content
                            className="site-layout-background"
                            style={{
                                padding: 24,
                                minHeight: 280,
                            }}
                        >
                            {
                                active_tab == 'my' &&
                                <Card>
                                    <UserCenter user={loginUser} />
                                </Card>
                            }
                            {
                                active_tab == 'role_list' &&
                                <Card>
                                    <RoleList />
                                </Card>
                            }
                            {
                                active_tab == 'user_list' &&
                                <Card>
                                    <UserList />
                                </Card>
                            }
                        </Content>
                    </Layout>
                </Layout>
            </Layout>

        )
    }
}

export default connect(() => {
    return {
        loginUser: getState().loginUser
    }
})(Admin)