import * as React from 'react'
import * as ReactDOM from 'react-dom'
import Admin from './components/Admin'

const container = document.getElementById('app')

ReactDOM.render(<Admin />, container)
