import { connect, getState } from '../store'
import RoleList, { RoleListProps } from '../components/RoleList'
import { updateRoleAuthority, addRole, delRole } from '../actions'

export default connect((props): RoleListProps => {
    const { authorities, roles, users } = getState()
    return {
        ...props,
        users,
        authorities, roles,
        updateRoleAuthority,
        addRole,
        delRole
    }
})(RoleList)