import { connect, getState } from '../store'
import UserCenter,{UserCenterProps} from '../components/UserCenter'
import { changePassword } from '../actions'

export default connect((props): UserCenterProps => {
    return {
        ...props,
        changePassword
    }
})(UserCenter)