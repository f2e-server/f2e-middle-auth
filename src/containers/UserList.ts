import { connect, getState } from '../store'
import UserList, { UserListProps } from '../components/UserList'
import { addUser, delUser, updateUserRole, updateUserLockIPs, updateUserExpires, resetPassword } from '../actions'

export default connect((props): UserListProps => {
    const { authorities, users, roles, loginUser } = getState()
    return {
        ...props,
        authorities, 
        users,
        roles,
        loginUser,
        addUser,
        delUser,
        updateUserRole,
        updateUserLockIPs,
        updateUserExpires,
        resetPassword,
    }
})(UserList)