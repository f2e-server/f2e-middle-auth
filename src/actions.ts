import { dispatch } from './store'
import * as api from './api'

export const init = () => api.allInfo().then(data => dispatch(state => ({...state, ...data})))
export const updateRoleAuthority = (roleId: number, authorityId: number, level: number) => api.updateRoleAuthority({roleId, authorityId, level}).then(init)
export const addRole = (name: string) => api.addRole(name).then(init)
export const delRole = (roleId: number) => api.delRole(roleId).then(init)
export const addUser = ({ name, password, nickname }) => api.addUser({ name, password, nickname }).then(init)
export const delUser = (userId: number) => api.delUser(userId).then(init)
export const updateUserRole = ({ userId, roleId }) => api.updateUserRole({ userId, roleId }).then(init)
export const updateUserLockIPs = ({ userId, ips }) => api.updateUserLockIPs({ userId, ips }).then(init)
export const updateUserExpires = ({ userId, expires }) => api.updateUserExpires({ userId, expires }).then(init)
export const loginUser = () => api.loginUser()
export const changePassword = ({ pw0, pw1, pw2 }) => api.changePassword({ pw0, pw1, pw2 })
export const resetPassword = (userId: number, password: string) => api.resetPassword(userId, password)

init()