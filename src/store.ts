import createStore, { IPreact } from 'ipreact-for-react'
import { StoreState } from './interface'
import { loginUser } from './api';

const initState: StoreState = {
    curTab: 'update_self',
    loginUser: null,
    authorities: [],
    roles: [],
    users: []
}
const { dispatch, connect, getState }: IPreact<StoreState> = createStore()(initState)

export { dispatch, connect, getState }

loginUser().then(user => dispatch(state => ({...state, loginUser: user})))