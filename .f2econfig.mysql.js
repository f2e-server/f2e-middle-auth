const { argv } = process
const build = argv[argv.length - 1] === 'build'
const dev = argv.includes('-O')
/**
 * 启动 Node.js 进程时传入的命令行参数是否有sql
 */
const lib = require('./lib')

const MysqlStoreMiddle = [
    conf => lib(conf, {
        onSessionChange: (sessions) => {
            console.log(sessions)
        },
        store: new lib.MysqlStore({
            host: '127.0.0.1',
            port: 3306,
            user: 'root',
            password: '123456',
            database: 'authority_test'
        }, {
            show_sql: true,
            // 权限
            authorities: {
                tablename: 'auth_authorities',
                columns: {
                    id: 'id',
                    name: 'name'
                }
            },
            // 角色
            roles: {
                tablename: 'auth_roles',
                columns: {
                    id: 'id',
                    name: 'name',
                    is_admin: 'is_admin'
                }
            },
            // 角色权限对应
            role_auth_rel: {
                tablename: 'auth_role_auth_rel',
                columns: {
                    id: 'id',
                    role_id: 'role_id',
                    auth_id: 'auth_id',
                    level: 'level'
                }
            },
            users: {
                tablename: 'auth_users',
                columns: {
                    id: 'id',
                    name: 'name',
                    nickname: 'nickname',
                    role_id: 'role_id',
                    password: 'password',
                    lock_ips: 'lock_ips',
                    expires: 'expires',
                    // is_sys: 'is_sys',
                }
            }
        }),
        dev
    })
]

module.exports = {
    livereload: !build,
    gzip: true,
    build,
    buildFilter: (p) => !p || /src/.test(p),
    middlewares: [
        dev && { middleware: 'esbuild', test: /^\/?lib/ },
    ].concat(MysqlStoreMiddle)
}
