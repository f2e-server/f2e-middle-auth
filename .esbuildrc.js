/**
 * @type { import('f2e-middle-esbuild').BuildOptions }
 */
let config = {
    watches: [/\.(tsx?|less)$/],
    sourcemap: 'external',
    entryPoints: ['src/index.tsx'],
    outfile: 'lib/bundle.js',
    target: 'chrome70',
    jsxFactory: 'React.createElement',
    bundle: true,
    format: 'iife',
    loader: {
        '.tsx': 'tsx',
        '.ts': 'ts',
    },
    tsconfig: './tsconfig.json',
};

module.exports = config