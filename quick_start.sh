mkdir f2e_auth_app && cd f2e_auth_app;

npm init -y;
npm i f2e-server f2e-middle-auth;
npm i less@4 --save-dev;

node ./node_modules/.bin/f2e conf;
sed -i '/middlewares:/a \\t{middleware: "auth"},' ./.f2econfig.js;
cp ./node_modules/f2e-middle-auth/.authority.json ./;
mkdir admin;
echo 'require("f2e-server")({});' > start.js;

node start.js;