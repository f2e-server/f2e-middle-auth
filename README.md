# f2e-middle-auth 
账户权限角色管理中间件

## 快捷体验
```sh
curl -o- https://gitee.com/f2e-server/f2e-middle-auth/raw/master/quick_start.sh | bash
```

```sh
wget -qO- https://gitee.com/f2e-server/f2e-middle-auth/raw/master/quick_start.sh | bash
```

## 使用方法
f2e-middle-auth 作为 [f2e-server](https://gitee.com/f2e-server/f2e-server) 的middle运行

- `npm install f2e-middle-auth`
- `cp ./node_modules/f2e-middle-auth/.authority.json`
- `.f2econfig.js` 添加配置
```TypeScript
module.exports = {
    ...
    middlewares: [{
        middleware: 'auth'
    }]
}
```
- 启动 f2e-server 服务, 打开浏览器自动跳转登录页面 (账号密码 admin/123456)  
![界面截图1](./images/login.png)  
![界面截图2](./images/password.png)  
![界面截图3](./images/roles.png)  
![界面截图4](./images/users.png)  

## 注意
当设置用户 `isSys` 为`true`的时候，`/admin/allInfo` 接口中的`users`数组中将不会出现该用户
## config
`.f2econfig.js`
```TypeScript
module.exports = {
    ...
    middlewares: [{
        middleware: 'auth',
        maxAge: 86400,  // session 超时时间
        test: /.*/,     // 拦截路径

        store?: Store    // 默认使用json文件保存信息

        layout_page?: string
        login_page?: string
        admin_page?: string

        login_url: 'login',     // 登录
        logout_url: 'logout',   // 登出
        admin_url: 'admin'      // 管理界面
    }]
}
```
## Store 配置
### 配置mysql管理

```TypeScript
const { MysqlStore } = require('f2e-middle-auth')
module.exports = {
    ...
    middlewares: [
        {
            middleware: 'auth',
            store: new lib.MysqlStore({
                host: '127.0.0.1',
                port: 3306,
                user: 'root',
                password: 'root',
                database: 'authority'
            }, {
                authorities: {
                    tablename: 'permission',
                    columns: {
                        id: 'id',
                        name: 'name'
                    }
                },
                roles: {
                    tablename: 'roles',
                    columns: {
                        id: 'id',
                        name: 'name',
                        is_admin: 'is_admin'
                    }
                },
                role_auth_rel: {
                    tablename: 'roles2permission',
                    columns: {
                        id: 'id',
                        role_id: 'role_id',
                        auth_id: 'permission_id',
                        level: 'level'
                    }
                },
                users: {
                    tablename: 'users',
                    columns: {
                        id: 'id',
                        name: 'email',
                        nickname: 'nickname',
                        role_id: 'role_id',
                        password: 'password'
                    }
                }
            })
        }
    ]
}
```

### 统一用户配置-Mysql数据库

``` TypeScript 
const { UniteMysqlStore } = require('f2e-middle-auth')
const systemConfig={
    SQL_CONFIG:{
        host: '127.0.0.1',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'now_sys_basic'
    },
    TABLE_CONFIG:{
        authorities: {
            tablename: 'auth_authorities',
            columns: {
                id: 'id',
                name: 'name'
            }
        },
        roles: {
            tablename: 'auth_roles',
            columns: {
                id: 'id',
                name: 'name',
                is_admin: 'is_admin'
            }
        },
        role_auth_rel: {
            tablename: 'auth_role_auth_rel',
            columns: {
                id: 'id',
                role_id: 'role_id',
                auth_id: 'auth_id',
                level: 'level'
            }
        },
        user_role: {
            tablename: 'auth_users',
            columns: {
                id: 'id',
                role_id: 'role_id',
                user_name: 'name'
            }
        }
    }
}
/**
 * 不传userConfig为默认配置，默认配置如下
 * 统一用户数据存储数据库，只支持mysql
*/
const userConfig={
    SQL_CONFIG:{
        host: '127.0.0.1',
        port: 3306,
        user: 'root',
        password: '123456',
        database: 'meta',        
    },
    TABLE_CONFIG:{
        users: {
            tablename: 'auth_users',
            columns: {
                id: 'id',
                name: 'name',
                nickname: 'nickname',
                password: 'password'
            }
        }
    }
}
module.exports = {
    ...
    middlewares: [
        {
            middleware: 'auth',
            store: new lib.UniteMysqlStore(systemConfig,userConfig)
        }
    ]
}
```

```SQL
CREATE TABLE `auth_authorities`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
CREATE TABLE `auth_role_auth_rel`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `role_id` int(0) NOT NULL,
  `auth_id` int(0) NOT NULL,
  `level` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
CREATE TABLE `auth_roles`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_admin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
CREATE TABLE `auth_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `lock_ips` varchar(255) DEFAULT NULL,
  `expires` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
INSERT INTO `auth_authorities` (`id`, `name`) VALUES (1,'测试权限');
INSERT INTO `auth_roles` (`id`, `name`, `is_admin`) VALUES (1,'管理员','1');
INSERT INTO `auth_users` (`id`, `role_id`, `name`, `nickname`, `password`, `lock_ips`, `expires`) VALUES (1,1,'admin','管理员','e10adc3949ba59abbe56e057f20f883e',NULL,NULL);
```

### mongodb配置

```  TypeScript 
import { MongoStore } from 'f2e-middle-auth'

const MongoStoreConf = {
    config: {
        url: ' mongodb://127.0.0.1:27017/',
        dbName: 'authority_test'
    },
    option: {
        authorities: {
            tablename: 'authorities',
            columns: {
                id: '_id',
                name: 'name'
            }
        },
        roles: {
            tablename: 'roles',
            columns: {
                id: '_id',
                name: 'name',
                is_admin: 'isAdmin',
                authorities: 'authorities'
            }
        },
        users: {
            tablename: 'users',
            columns: {
                id: '_id',
                name: 'name',
                nickname: 'nickname',
                role_id: 'role_id',
                isSys: 'isSys',
                password: 'password',
                lock_ips: 'lock_ips',
                expires: 'expires'
            }
        }
    }
}
```

#### mongodb 初始化脚本

``` javaScript
    /**
     * 用户表
     */
    db.getCollection("users").insert([ {
        _id: ObjectId("5f40faef0c0a000045004ce3"),
        name: "admin",
        nickname: "系统管理员",
        password: "e10adc3949ba59abbe56e057f20f883e",
        isSys: true,
        roleId: ObjectId("5f40fa160c0a000045004ce2")
    } ]);
    /**
     * 
     * 角色表
    */
   db.getCollection("roles").insert([ {
        _id: ObjectId("5f40fa160c0a000045004ce2"),
        name: "管理员",
        isSys: true,
        isAdmin: true,
        authorities: { }
    } ]);
    /**
     * 
     * 权限表
    */
   db.getCollection("authorities").insert([ {
        _id: ObjectId("5f40f5ffff580000de003b76"),
        name: "测试权限"
    } ]);
```