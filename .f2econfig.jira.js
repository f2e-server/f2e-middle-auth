const { argv } = process
const build = argv[argv.length - 1] === 'build'
const dev = argv.includes('-O')

const lib = require('./lib')

const testMongo = [
    conf => lib(conf, {
        store: new lib.JiraMongoStore({
            url: ' mongodb://127.0.0.1:27017/',
            dbName: 'authority_test',
            jiraRoot: 'http://atlassian.yourcompany.com/jira/',
            apiRoot: 'apiRoot'
        },
        // {
        //     authorities: {
        //         tablename: 'authorities',
        //         columns: {
        //             id: '_id',
        //             name: 'name'
        //         }
        //     },
        //     roles: {
        //         tablename: 'roles',
        //         columns: {
        //             id: '_id',
        //             name: 'name',
        //             is_admin: 'isAdmin',
        //             isSys: 'isSys',
        //             authorities: 'authorities'
        //         }
        //     },
        //     role_auth_rel: {
        //         tablename: 'role_auth_rel',
        //         columns: {
        //             id: '_id',
        //             role_id: 'role_id',
        //             auth_id: 'auth_id',
        //             level: 'level'
        //         }
        //     },
        //     users: {
        //         tablename: 'users',
        //         columns: {
        //             id: '_id',
        //             name: 'name',
        //             nickname: 'nickname',
        //             role_id: 'roleId',
        //             password: 'password',
        //             lock_ips: 'lock_ips'
        //         }
        //     }
        // }
        ),
        dev
    })
]

module.exports = {
    livereload: !build,
    gzip: true,
    build,
    buildFilter: (p) => !p || /src/.test(p),
    middlewares: [
        dev && { middleware: 'esbuild', test: /^\/?lib/ },
    ].concat(testMongo)
}
