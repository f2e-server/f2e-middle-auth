// @ts-check
/**
 * @type {import('f2e-server').F2EConfig}
 */
const config = {
    build: true,
    buildFilter: (p) => !p || /src/.test(p),
    outputFilter: (p) => !p || /lib/.test(p),
    middlewares: [
        { middleware: 'esbuild', test: /^\/?lib/ },
    ],
    output: require('path').join(__dirname, './')
}
module.exports = config